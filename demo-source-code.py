import os
import sys
import webbrowser
import speech_recognition
from gtts import gTTS

class Colors:
  header = '\033[95m'
  blue = '\033[94m'
  cyan = '\033[96m'
  green = '\033[92m'
  warning = '\033[93m'
  fail = '\033[91m'
  end = '\033[0m'
  bold = '\033[1m'
  underline = '\033[4m'

url_prefix = 'https://'

site_domains = {
  '臉書': 'facebook.com',
  'youtube': 'youtube.com',
  'gmail': 'mail.google.com',
  'google搜尋': 'google.com',
  'google地圖': 'maps.google.com',
  'google相簿': 'photos.google.com',
  'google日曆': 'calendar.google.com',
  '台灣銀行': 'www.bot.com.tw',
  '東森新聞': 'news.ebc.net.tw',
  '家樂福': 'online.carrefour.com.tw',
  '中央氣象局': 'www.cwb.gov.tw/V8/C',
  '雅虎字典': 'tw.dictionary.search.yahoo.com',
}

wait_count = 0

response_filename = 'chatting.mp3'

recognizer = speech_recognition.Recognizer()
microphone = speech_recognition.Microphone()

# 在此僅提供此函式的部分程式碼。
def respond(message):
  audio_file = open(responses_filename, 'wb')
   ⋮
  audio_file.close()
   ⋮

# 在此僅提供此函式的部分程式碼。
def recognize():
  global wait_count
   ⋮
  try:
    with microphone as source:
     ⋮
  except KeyboardInterrupt:
    return '結束'
   ⋮

def main():
  global wait_count

  print('本應用程式提供，語音瀏覽 Youtube、臉書、東森新聞、Google搜尋、臺灣銀行、Google地圖、 Gmail、Google相簿、家樂福、中央氣象局、Google日曆、雅虎字典等 12 個網站首頁的機制。')

  while True:
    if wait_count == 0:
      print('\n您欲瀏覽的網站首頁是 (「結束」可終止本程式)？')
      sys.stdout.flush()

    phrase = recognize()
    
    if '結束' in phrase  or wait_count > 5:
      print(f'\n{Colors.green}好的！已結束了本程式。{Colors.end}')
      respond('好的！已結束了本程式。')
      break
    elif phrase in site_domains:
      webbrowser.open(url_prefix + site_domains[phrase])
      print(f'{Colors.green}好的，已開啟了該首頁。{Colors.end}')
      respond('已開啟了該網站首頁。')

      wait_count = 0
    elif phrase == '語音有誤':
      if wait_count == 1:
        print(f'{Colors.warning}等待辨識中 .{Colors.end}', end = '')
        sys.stdout.flush()
      else:
        print('.', end = '')
        sys.stdout.flush()
      
      respond('等待辨識中。')
    else:
      print(f'\n{Colors.warning}「{phrase}」，並非本應用程式的支援範圍！{Colors.end}')
      respond(f'{phrase}，並非本應用程式的支援範圍！')
      wait_count = 0

main()
