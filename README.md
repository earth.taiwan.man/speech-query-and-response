本應用程式，總共使用到 os、sys、webbrowser、speech_recognition、gtts 等 6 個模組。其中：

- os 模組當中的特定函式，可間接播放系統所支援格式的音效檔。
- sys 模組當中的特定函式，可將緩衝區中的文字傳訊，立即顯示在螢幕上。
- webbrowser 模組當中的特定函式，可用來開啟特定網站的首頁。
- speech_recognition 模組的多個函式，則是實現本應用程式的語音辨識功能的主要部分。
- gtts 模組的特定函式，則可將文字訊息，轉換成為語音訊號，方便使用者加以聆聽。

### $`\textcolor{GoldenRod}{\text{載入相關模組。}}`$
```
import os
import sys
import webbrowser
import speech_recognition
from gtts import gTTS
```

### $`\textcolor{GoldenRod}{\text{以類別裡面的資料成員之形式，加以定義用於命令列模式下的文字訊息之顏色。}}`$
```
class Colors:
  header = '\033[95m'
  blue = '\033[94m'
  cyan = '\033[96m'
  green = '\033[92m'
  warning = '\033[93m'
  fail = '\033[91m'
  end = '\033[0m'
  bold = '\033[1m'
  underline = '\033[4m'
```

### $`\textcolor{GoldenRod}{\text{定義各個網站網址的前綴字串，也就是通訊協定代碼，和冒號與正斜線。}}`$
`url_prefix = 'https://'`

### $`\textcolor{GoldenRod}{\text{以字典資料型態的形式，加以定義可供使用者語音控制並瀏覽的網站首頁。}}`$
```
site_domains = {
  '臉書': 'facebook.com',
  'youtube': 'youtube.com',
  'gmail': 'mail.google.com',
  'google搜尋': 'google.com',
  'google地圖': 'maps.google.com',
  'google相簿': 'photos.google.com',
  'google日曆': 'calendar.google.com',
  '台灣銀行': 'www.bot.com.tw',
  '東森新聞': 'news.ebc.net.tw',
  '家樂福': 'online.carrefour.com.tw',
  '中央氣象局': 'www.cwb.gov.tw/V8/C',
  '雅虎字典': 'tw.dictionary.search.yahoo.com',
}
```

### $`\textcolor{GoldenRod}{\text{用來記錄系統等待辨識的次數。達 5 次時，會使得本應用程式自動結束。}}`$
`wait_count = 0`

### $`\textcolor{GoldenRod}{\text{本應用程式會將每次播放給使用者的語音訊息，暫存於 chatting.mp3 檔案裡面。}}`$
`response_filename = 'chatting.mp3'`

### $`\textcolor{GoldenRod}{\text{繫結所需的辨識機制和麥克風輸入。}}`$
```
recognizer = speech_recognition.Recognizer()
microphone = speech_recognition.Microphone()
```
### $`\textcolor{GoldenRod}{\text{定義「用來播放語音訊息給使用者」的自訂函式。}}`$
```
# 此函式具有 6 列程式碼，在此僅提供其一小部分。
def respond(message):
  audio_file = open(responses_filename, 'wb')
   ⋮
  audio_file.close()
   ⋮
```

### $`\textcolor{GoldenRod}{\text{定義「用來辨識使用者口說之語音」的函式。}}`$
```
# 此函式具有 24 列程式碼，在此僅提供其一小部分。
def recognize():
  global wait_count
   ⋮
  try:
    with microphone as source:
     ⋮
  except KeyboardInterrupt:
    return '結束'
   ⋮
```

### $`\textcolor{GoldenRod}{\text{定義「串聯其他瑣碎事務」的主函式。}}`$
```
# 此函式具有 35 列程式碼。
def main():
  global wait_count

  print('本應用程式提供，語音瀏覽 Youtube、臉書、東森新聞、Google搜尋、臺灣銀行、Google地圖、
  Gmail、Google相簿、家樂福、中央氣象局、Google日曆、雅虎字典等 12 個網站首頁的機制。')

  while True:
    if wait_count == 0:
      print('\n您欲瀏覽的網站首頁是 (「結束」可終止本程式)？')
      sys.stdout.flush()

    phrase = recognize()
    
    if '結束' in phrase  or wait_count > 5:
      print(f'\n{Colors.green}好的！已結束了本程式。{Colors.end}')
      respond('好的！已結束了本程式。')
      break
    elif phrase in site_domains:
      webbrowser.open(url_prefix + site_domains[phrase])
      print(f'{Colors.green}好的，已開啟了該首頁。{Colors.end}')
      respond('已開啟了該網站首頁。')

      wait_count = 0
    elif phrase == '語音有誤':
      if wait_count == 1:
        print(f'{Colors.warning}等待辨識中 .{Colors.end}', end = '')
        sys.stdout.flush()
      else:
        print('.', end = '')
        sys.stdout.flush()
      
      respond('等待辨識中。')
    else:
      print(f'\n{Colors.warning}「{phrase}」，並非本應用程式的支援範圍！{Colors.end}')
      respond(f'{phrase}，並非本應用程式的支援範圍！')
      wait_count = 0
```

### $`\textcolor{GoldenRod}{\text{呼叫並執行主函式。}}`$
`main()`

### $`\textcolor{GoldenRod}{\text{該作品的參考網址}}`$

[語音 導覽 特定網站 的 首頁](https://gitlab.com/earth.taiwan.man/speech-query-and-response/-/blob/main/demo-source-code.py)
